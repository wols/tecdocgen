# README

## TecDocGen

**Tec**hnical **Doc**umentation **Gen**erator - A personal [Antora](https://antora.org/) workchain

https://tecdocgen.gitlab.io/tecdocgen/tecdocgen/main/

![TecDocGen flow](https://tecdocgen.gitlab.io/tecdocgen/tecdocgen/main/_images/diagram/flow-tecdocgen.svg "TecDocGen flow")

**IMPORTANT**

    This software is under the heavy development and considered ALPHA
    quality till the version hits >=1.0.0.
    Things might be broken, not all features have been implemented and
    APIs will be likely to change.

    YOU HAVE BEEN WARNED.

***

TecDocGen © 2019-2023 WOLfgang Schricker
