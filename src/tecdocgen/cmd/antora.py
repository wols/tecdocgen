"""TecDocGen antora command."""

import click


@click.command()
@click.argument(
    'playbook',
    type=click.File('r'),
    required=True,
)
def antora(playbook):
    """Generate documentation from Antora playbook file."""
    pass
