"""
Snyk test integration.

https://snyk.io/

npm install -g snyk
npm install -g snyk-python-plugin

snyk --help
snyk auth

pipenv graph > requirements-dev.txt

snyk test --file=requirements-dev.txt [--package-manager=pip]
"""

import os
import pytest
import re
import subprocess
import unittest


@pytest.mark.usefixtures('f_snyk')
class TestSnyk(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._snyk = pytest.f_snyk
        try:
            __completed_process = subprocess.run(
                ['pipenv', 'graph'],
                stdout=open(cls._snyk['file'], 'w'),
            )
        except subprocess.CalledProcessError as e:
            raise RuntimeError(e)

    @classmethod
    def tearDownClass(cls):
        if os.path.exists(cls._snyk['file']):
            os.remove(cls._snyk['file'])

    def test_0_requirements(self) -> None:
        self.assertTrue(os.path.exists(self._snyk['file']))

    @pytest.mark.dependency()
    def test_1_snyk(self) -> None:
        try:
            __completed_process = subprocess.run(
                self._snyk['command'],
                capture_output=True,
                text=True,
            )
        except subprocess.CalledProcessError as e:
            raise RuntimeError(e)

        self.assertFalse(__completed_process.stderr)
        self.assertTrue(__completed_process.stdout)
        self._snyk['stdout'] = __completed_process.stdout
        # print('DEBUG', self._snyk['stdout'])

    @pytest.mark.dependency(depends=['TestSnyk::test_1_snyk'])
    def test_3_vulnerable(self) -> None:
        __result = re.search(
            r'Tested (\d+) dependencies for known issues, (\w+) vulnerable paths found.',
            self._snyk['stdout'],
        )
        [__dependencies, __vulnerable] = __result.group(1, 2)

        self.assertEqual(__vulnerable, 'no')
        print('DEBUG', __vulnerable + '/' + __dependencies)
