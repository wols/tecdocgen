import os
import pytest
import unittest
from tecdocgen import util


class Test_tmpdir(unittest.TestCase):
    def test_0_method(self) -> None:
        self.assertTrue('tmpdir' in dir(util))

    @pytest.mark.dependency()
    def test_1_tmpdir(self) -> None:
        with util.tmpdir() as __tmpd:
            print('DEBUG', __tmpd)
            self.assertRegex(os.path.basename(__tmpd), r'^tecdocgen\.util\.')
            self.assertTrue(os.path.isdir(__tmpd))
            self.assertTrue(os.path.exists(__tmpd))
        self.assertFalse(os.path.exists(__tmpd))
