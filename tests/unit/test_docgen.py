import pytest
import unittest
from tecdocgen import docgen


@pytest.mark.usefixtures('f_docgen_state')
class TestGenStateDry(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._state = docgen.GenState()

    def test_00_class(self) -> None:
        self.assertIsInstance(self._state, docgen.GenState)

    def test_01_attribute(self) -> None:
        for __attr in ['current', 'dry']:
            with self.subTest(__attr=__attr):
                self.assertTrue(hasattr(self._state, __attr), __attr)

    def test_03_dry(self) -> None:
        self.assertFalse(self._state.dry)

        with self.assertRaises(AttributeError) as __exc:
            self._state.dry = 1
        self.assertEqual(
            str(__exc.exception),
            'dry must True or False',
        )

        self._state.dry = True
        self.assertTrue(self._state.dry)

    def test_10_instanced(self) -> None:
        self.assertEqual(self._state.current, pytest.f_docgen_state[10]['id'])
        self.assertListEqual(
            sorted([t.name for t in self._state.allowed_events]),
            sorted(pytest.f_docgen_state[10]['allowed_events']),
        )

    def test_80_canceled(self) -> None:
        self._state.cancel()
        self.assertEqual(self._state.current, pytest.f_docgen_state[80]['id'])
        self.assertListEqual(
            sorted([t.name for t in self._state.allowed_events]),
            sorted(pytest.f_docgen_state[80]['allowed_events']),
        )

    def test_90_cleaned(self) -> None:
        self._state.clean()
        self.assertEqual(self._state.current, pytest.f_docgen_state[90]['id'])
        self.assertListEqual(
            sorted([t.name for t in self._state.allowed_events]),
            sorted(pytest.f_docgen_state[90]['allowed_events']),
        )

    @pytest.mark.dependency()
    def test_99_dry(self) -> None:
        self.assertTrue(self._state.dry)


@pytest.mark.dependency(
    depends=['TestGenStateDry::test_99_dry'],
    scope='module',
)
class TestGenState(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._state = docgen.GenState()

    def test_00_class(self) -> None:
        self.assertIsInstance(self._state, docgen.GenState)

    @pytest.mark.dependency()
    def test_99_pass(self) -> None:
        self.assertTrue(True)


@pytest.mark.dependency(
    depends=['TestGenState::test_99_pass'],
    scope='module',
)
class TestGenerator(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._gen = docgen.Generator()

    def test_0_class(self) -> None:
        self.assertIsInstance(self._gen, docgen.Generator)

    def test_1_attribute(self) -> None:
        for __attr in ['state']:
            with self.subTest(__attr=__attr):
                self.assertTrue(hasattr(self._gen, __attr))

    @pytest.mark.dependency()
    def test_9_pass(self) -> None:
        pass
