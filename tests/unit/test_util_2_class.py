import os
import pytest
import shutil
import sys
import unittest
from packaging.version import Version
from tecdocgen import util


@pytest.mark.usefixtures('f_adoc', 'f_util_versions')
class TestAsciidoctor(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._util   = util.Asciidoctor()
        cls.version = pytest.f_util_versions['asciidoctor']

    def test_0_class(self) -> None:
        self.assertIsInstance(self._util, util.Util)
        self.assertIsInstance(self._util, util.Asciidoctor)

    def test_1_attribute(self) -> None:
        for __attr in ['file', 'pushd', 'run']:
            with self.subTest(__attr=__attr):
                self.assertTrue(hasattr(self._util, __attr))

    @pytest.mark.dependency(
        depends=['tests/unit/test_util_1_method.py::Test_tmpdir::test_1_tmpdir'],
        scope='session',
    )
    def test_3_pushd(self) -> None:
        self.assertIsNone(self._util.pushd)
        self._util.pushd = 'missing_dir'
        self.assertEqual(self._util.pushd, 'missing_dir')

        with util.tmpdir() as __tmpd:
            print('DEBUG', __tmpd)
            self._util.pushd = __tmpd
            self.assertEqual(self._util.pushd, __tmpd)

    @pytest.mark.dependency()
    def test_5_executable(self) -> None:
        self.assertTrue(self._util.executable)

    @pytest.mark.dependency(depends=['TestAsciidoctor::test_5_executable'])
    def test_7_version(self) -> None:
        print('DEBUG', self.version, '[' + self._util.version + ']')
        self.assertTrue(Version(self._util.version) == Version(self.version))

    @pytest.mark.dependency(depends=['TestAsciidoctor::test_5_executable'])
    def test_9_run(self) -> None:
        with self.assertRaises(RuntimeError) as __exc:
            self._util.pushd = None
            self._util.file  = None
            self.assertIsNone(self._util.run())
        self.assertEqual(
            str(__exc.exception),
            "no file given",
        )

        self._util.file  = pytest.f_adoc
        __file_result    = pytest.f_html
        self.assertIsNone(self._util.run())
        self.assertTrue(os.path.exists(__file_result))
        if os.path.exists(__file_result):
            print('DEBUG', 'delete', __file_result)
            os.remove(__file_result)
            self.assertFalse(os.path.exists(__file_result))


@pytest.mark.dependency(depends=['TestAsciidoctor::test_9_run'])
@pytest.mark.usefixtures('f_antora_module', 'f_util_versions')
class TestAsciidoctorDiagram(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._util   = util.AsciidoctorDiagram()
        cls.version = pytest.f_util_versions['asciidoctor']

    def test_0_class(self) -> None:
        self.assertIsInstance(self._util, util.Util)
        self.assertIsInstance(self._util, util.AsciidoctorDiagram)

    def test_1_attribute(self) -> None:
        for __attr in ['file', 'pushd', 'run']:
            with self.subTest(__attr=__attr):
                self.assertTrue(hasattr(self._util, __attr))

    def test_3_pushd(self) -> None:
        self.assertIsNone(self._util.pushd)

        with self.assertRaises(AttributeError) as __exc:
            self._util.pushd = 'missing_dir'
        self.assertIn(
            str(__exc.exception), [
                # Python 3.10
                "can't set attribute 'pushd'",
                # Python 3.11
                "property 'pushd' of 'AsciidoctorDiagram' object has no setter",
            ]
        )

        self._util.file = os.path.join(
            pytest.f_antora_module_diagrams,
            pytest.f_antora_module_diagrams_file,
        )
        self.assertEqual(self._util.pushd, pytest.f_antora_module_diagrams)

    def test_5_executable(self) -> None:
        self.assertTrue(self._util.executable)

    def test_7_version(self) -> None:
        print('DEBUG', self.version, '[' + self._util.version + ']')
        self.assertTrue(Version(self._util.version) == Version(self.version))

    def test_9_run(self) -> None:
        self.assertEqual(self._util.pushd, pytest.f_antora_module_diagrams)
        self.assertEqual(self._util.file, pytest.f_antora_module_diagrams_file)
        self.assertIsNone(self._util.run())
        __file_result = 'docs/modules/ROOT/images/diagram/overview.svg'
        self.assertTrue(os.path.exists(__file_result))


@pytest.mark.usefixtures('f_util_versions')
class TestAsciidoctorPDF(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._util   = util.AsciidoctorPDF()
        cls.version = pytest.f_util_versions['asciidoctor-pdf']

    def test_0_class(self) -> None:
        self.assertIsInstance(self._util, util.Util)
        self.assertIsInstance(self._util, util.AsciidoctorPDF)

    def test_1_attribute(self) -> None:
        for __attr in ['file', 'pdf', 'pushd', 'run']:
            with self.subTest(__attr=__attr):
                self.assertTrue(hasattr(self._util, __attr))

    def test_3_pushd(self) -> None:
        self.assertIsNone(self._util.pushd)
        with self.assertRaises(AttributeError) as __exc:
            self._util.pushd = 'missing_dir'
        self.assertIn(
            str(__exc.exception), [
                # Python 3.10
                "can't set attribute 'pushd'",
                # Python 3.11
                "property 'pushd' of 'AsciidoctorPDF' object has no setter",
            ]
        )

    @pytest.mark.dependency()
    def test_5_executable(self) -> None:
        self.assertTrue(self._util.executable)

    @pytest.mark.dependency(depends=['TestAsciidoctorPDF::test_5_executable'])
    def test_7_version(self) -> None:
        print('DEBUG', self.version, '[' + self._util.version + ']')
        self.assertTrue(Version(self._util.version) == Version(self.version))

    @pytest.mark.dependency(depends=['TestAsciidoctorPDF::test_5_executable'])
    def test_9_run(self) -> None:
        __file_adoc   = 'docs/modules/ROOT/pages/development.adoc'
        __file_result = 'docs/modules/ROOT/attachments/development.pdf'
        self._util.file = __file_adoc
        self.assertEqual(self._util.file, __file_adoc)
        self._util.pdf = __file_result
        self.assertEqual(self._util.pdf, __file_result)
        self.assertIsNone(self._util.run())
        self.assertTrue(os.path.exists(__file_result))
        if os.path.exists(__file_result):
            print('DEBUG', 'delete', __file_result)
            os.remove(__file_result)
            self.assertFalse(os.path.exists(__file_result))


@pytest.mark.usefixtures('f_antora_playbook', 'f_util_versions')
class TestAntora(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._util   = util.Antora()
        cls.version = pytest.f_util_versions['antora']

    def test_0_class(self) -> None:
        self.assertIsInstance(self._util, util.Util)
        self.assertIsInstance(self._util, util.Antora)

    def test_1_attribute(self) -> None:
        for __attr in ['playbook', 'pushd', 'run']:
            with self.subTest(__attr=__attr):
                self.assertTrue(hasattr(self._util, __attr))

    def test_3_pushd(self) -> None:
        self.assertIsNone(self._util.pushd)
        with self.assertRaises(AttributeError) as __exc:
            self._util.pushd = 'missing_dir'
        self.assertIn(
            str(__exc.exception), [
                # Python 3.10
                "can't set attribute 'pushd'",
                # Python 3.11
                "property 'pushd' of 'Antora' object has no setter",
            ]
        )
        self._util.playbook = pytest.f_antora_playbook
        self.assertEqual(self._util.pushd, 'docs')

    @pytest.mark.dependency()
    def test_5_executable(self) -> None:
        self.assertTrue(self._util.executable)

    @pytest.mark.dependency(depends=['TestAntora::test_5_executable'])
    def test_7_version(self) -> None:
        print('DEBUG', self.version, '[' + self._util.version + ']')
        self.assertTrue(Version(self._util.version) == Version(self.version))

    @pytest.mark.dependency(depends=['TestAntora::test_5_executable'])
    def test_9_run(self) -> None:
        __head_tail = os.path.split(pytest.f_antora_playbook)
        self.assertEqual(self._util.pushd, __head_tail[0])
        self.assertEqual(self._util.playbook, __head_tail[1])
        self.assertIsNone(self._util.run())
        __dir_result = 'docs/_preview'
        self.assertTrue(os.path.exists(__dir_result))
        if os.path.exists(__dir_result):
            print('DEBUG', 'delete', __dir_result)
            shutil.rmtree(__dir_result)
            self.assertFalse(os.path.exists(__dir_result))


class TestMain(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # reset argv to hide test arguments eg. 'pytest -s'
        sys.argv = []
        cls._util = util.Main()

    def test_0_class(self) -> None:
        self.assertIsInstance(self._util, util.Main)

    def test_9_main(self) -> None:
        __result = self._util.run()
        self.assertIsNone(__result)
