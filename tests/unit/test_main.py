import pytest
import unittest
from tecdocgen import main


class TestMain(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._main = main.TecDocGen()

    def test_0_class(self) -> None:
        self.assertIsInstance(self._main, main.TecDocGen)
